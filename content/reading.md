+++
title = "Reading List"
date = "2014-04-09"
sidemenu = "true"
description = "A list of cool things to read"

+++

## Interesting Blogs
* [Pluskids](http://freemind.pluskid.org/)
* [当然我在扯淡](http://www.yinwang.org/)
* [codeCapsule](http://codecapsule.com/)
* [The morning paper](https://blog.acolyer.org/)

## Materials
* [Optimization and assumptions](http://freemind.pluskid.org/misc/optimization-and-assumptions/)

## Papers
* Range Thresholding on Streams 


