+++
title = "About"
date = "2016-09-02"
sidemenu = "true"
description = "About me and this site"
+++

## About Me


I am currently a Ph.D student in computer science. I am interested in spatial database.

I am also learning and practising guitar. 

## About This Site

I post articles here to record my ideas about research & life.

This site is

* generated with Hugo.
* hosted on Bitbucket.
* using the theme [blackburn](https://github.com/yoshiharuyamashita/blackburn).

Thanks for reading!
